import unittest

import hypnotoad


class ShuffleTestCase(unittest.TestCase):

    def test_conserve_element(self):
        items = [[1]*1, [2]*2, [3]*3]
        shuffled = hypnotoad.human_shuffle(items)

        self.assertEqual(len(shuffled), 6)
        
        self.assertEqual(len([x for x in shuffled if x == 1]), 1)
        self.assertEqual(len([x for x in shuffled if x == 2]), 2)
        self.assertEqual(len([x for x in shuffled if x == 3]), 3)

if __name__ == '__main__':
    unittest.main()
