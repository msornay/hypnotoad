import argparse
import itertools
import os
import random
import subprocess
import tempfile
import xml.etree.ElementTree as xml_et

# XXX use mimetype python-magic and/or a signature database 
_VIDEO_EXTENSION = ['.wmv', '.mp4', '.flv', '.mpeg', '.mpg', '.avi', '.mkv', ]

def human_shuffle(categorized_items):
    """ Shuffle items but tries to spread categories evenly (so that it is
    unlikely to find the same category several time in a row in the result)

    categorized_items should be a list of lists :
    [[A1, A2, ...], [B1, B2, ...], ...]

    Inspired by the spotify shuffle algorithm described here :
    http://labs.spotify.com/2014/02/28/how-to-shuffle-songs/

    """

    with_labels = []
    for items in categorized_items:
        items = list(items)               # copy
        random.shuffle(items)

        # distribute items randomly/evenly in [0.0, 1.0)
        gap = 1.0 / len(items)
        labels = [gap*(i + random.random()) for i, x in enumerate(items)]

        with_labels.append(zip(items, labels))

    # since sorted() is stable we might have some sort of bias with equal
    # labels (unlikely, but it doesn't hurt)
    random.shuffle(with_labels)

    # benchmark before telling me this merge is inefficient
    merged = sorted(itertools.chain(*with_labels), key=lambda x: x[1])

    return [x for x, label in merged]

def to_xspf_tree(playlist):
    root_element = xml_et.Element('playlist',
                                  {'version': '1.0',
                                   'xmlns': 'http://xspf.org/ns/0/'})
    root_element.append(xml_et.Comment('Generated with hypnotoad.py '
                                       'ALL GLORY TO THE HYPNOTOAD'))
    tracklist_element = xml_et.SubElement(root_element, 'trackList')
    for track in playlist:
        track_element = xml_et.SubElement(tracklist_element, 'track')
        location_element = xml_et.SubElement(track_element, 'location')
        location_element.text = 'file://'+track

    return xml_et.ElementTree(root_element)

def is_video(filename):
    filename = filename.lower()
    for ext in _VIDEO_EXTENSION:
        if filename.endswith(ext):
            return True
    return False


def list_videos(directory):
    """ Returns the list of all the videos found in the directory and its
    subdirectories (no depth limit ; doesn't follow symlinks) """

    videos = []

    for dirpath, dirnames, filenames in os.walk(directory):
        videos.extend([os.path.join(dirpath, x)
                       for x in filenames if is_video(x)])

    return videos
    
    
def categorize_by_subdir(path):
    dirpath, dirnames, filenames = next(os.walk(path))

    items = []
        
    # videos at the toplevel forms a category
    dirnames.append(dirpath)
    
    for d in dirnames:
        videos = list_videos(os.path.join(dirpath, d))
        if len(videos) > 0:
            items.append(videos)

    return items
        
def playlist(toplevel_dirs):
    items = []
    for d in toplevel_dirs:
        items.extend(categorize_by_subdir(d))
    return human_shuffle(items)


if __name__ == '__main__':

    parser = argparse.ArgumentParser("XXX")
    parser.add_argument('directories', nargs='+', metavar='DIRECTORY')
    parser.add_argument('-c', '--count', type=int)
    
    args = parser.parse_args()
    
    pl = playlist(args.directories)

    if args.count is not None:
        pl = pl[:args.count]

    tree = to_xspf_tree(pl)

    xspf_fd, xspf_path = tempfile.mkstemp(prefix="hypnotoad-", suffix='.xspf')
    xspf_file = os.fdopen(xspf_fd, "wb")
    tree.write(xspf_fd)

    subprocess.Popen(['vlc', '--quiet', xspf_path])

